# Assignment

## What I decided to solve

Sub Assignment (1): _Implement a parallel version of the method:_ `Matrix operator* (const Matrix& m1, const Matrix& m2)`

Features:
- the implemented method can at your own choice either work only for matrices or doubles or for matrices with coefficients of any type.

- the user should be able to decide at runtime wether to use the parallel or sequential version of the method

- parallelization can be implemented with any approach, but motivation for the choice made must be included